#include <stdio.h>

void pattern(int);
void Row(int);
int N=1,r;


void pattern(int x){    
    if(x>0)
    {
        Row(N);
        printf("\n");
        N++;
        pattern(x-1);   
    }
}


void Row(int y){
    if(y>0)
    {
        printf("%d",y);
        Row(y-1);
    }
}

int main(){
    printf("Enter a number: ");
    scanf("%d",&r);
    pattern(r);
    return 0;
}
